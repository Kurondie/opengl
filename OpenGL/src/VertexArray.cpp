#include "VertexArray.h"

#include <GL/glew.h>

namespace ge {

VertexLayout::VertexLayout() : _stride(0) {

}

void VertexLayout::addElement(const VertexLayoutElement& element) {
	_elements.push_back(element);
	_stride += element.count * sizeof(float);
}

const std::vector<VertexLayoutElement>& VertexLayout::getElements() const {
	return _elements;
}

unsigned int VertexLayout::getStride() const {
	return _stride;
}

unsigned int VertexLayout::getCount() const
{
	unsigned int count = 0;

	for (unsigned int i = 0; i < _elements.size(); i++) {
		count += _elements[i].count;
	}

	return count;
}


VertexArray::VertexArray() {
	glGenVertexArrays(1, &_vao);
	bind();
}

VertexArray::~VertexArray() {
	glDeleteVertexArrays(1, &_vao);
}

void VertexArray::bind() const {
	glBindVertexArray(_vao);
}

void VertexArray::unbind() const {
	glBindVertexArray(0);
}

// TODO : Manage multiple vertexbuffers inside a vertex array
void VertexArray::addVertexArray(const std::shared_ptr<VertexBuffer>& vb, const VertexLayout& layout) {
	bind();
	vb->bind();

	const std::vector<VertexLayoutElement>& elements = layout.getElements();
	unsigned int offset = 0;
	for (unsigned int i = 0; i < elements.size(); i++) {
		VertexLayoutElement element = elements[i];

		//GLint attribPosition = glGetAttribLocation(i, element.shaderAttrib.c_str());
		glEnableVertexAttribArray(i);
		glVertexAttribPointer(i, element.count, GL_FLOAT, GL_FALSE, layout.getStride(), (void*)offset);
		offset += element.count * sizeof(float);
	}
}

unsigned int VertexArray::getID() const
{
	return _vao;
}


} // ge