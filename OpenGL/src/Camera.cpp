#include "Camera.h"

#include <glm/gtc/matrix_transform.hpp>

Camera::Camera(float fov, float aspect, float near, float far) :
	_fov(fov),
	_aspect(aspect),
	_near(near),
	_far(far),

	_projection(glm::perspective(glm::radians(_fov), _aspect, _near, _far) ),
	_view(1.f),
	_projectionView(_projection),

	_position(0.f),
	_rotations(0.f, 0.f, 0.f, 0.f)
{
	update();
}

Camera::~Camera() {}


void Camera::setPosition(const glm::vec3& position) {
	_position = position;
	update();
}

const glm::vec3& Camera::getPosition() const {
	return _position;
}

void Camera::translate(const glm::vec3& move) {
	_position += move;
	update();
}


void Camera::setRotation(const glm::vec3& axis, float angle) {
	_rotations = glm::angleAxis(glm::radians(angle), axis);
	update();
}

void Camera::rotate(const glm::vec3& axis, float angle) {
	_rotations = glm::rotate(_rotations, glm::radians(angle), axis);
	update();
}

const glm::mat4& Camera::getProjectionView() const {
	return _projectionView;
}

const glm::quat& Camera::getQuat() const {
	return _rotations;
}


void Camera::update() {
	glm::mat4 transform = glm::translate(glm::mat4(1.f), _position);
	transform = transform * glm::toMat4(_rotations);
	_view = glm::inverse(transform);


	_projectionView = _projection * _view;
}
