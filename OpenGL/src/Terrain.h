#pragma once

#include "Mesh.h"

#include <memory>

#include "VertexArray.h"
#include "VertexBuffer.h"
#include "ElementBuffer.h"

class Terrain : public Mesh {
public:
	Terrain(unsigned int n);
	~Terrain();

	void generateFlatTerain();
	void initializeDiamondSquare();
	void diamondSquareAlgorithm(unsigned int x1, unsigned int z1, unsigned int x2, unsigned int z2);

private:
	virtual MeshVertices createVertices() override;
	virtual MeshTriangles createTriangles() override;

private:
	unsigned int _width, _height;

	MeshVertices _vertices;
	MeshTriangles _triangles;
};