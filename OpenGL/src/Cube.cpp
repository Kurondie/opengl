#include "Cube.h"

#include <glm/gtc/matrix_transform.hpp>

Cube::Cube() : 
	_verticesData(nullptr),
	_trianglesData(nullptr)
{
	build();

	delete[] _verticesData;
	delete[] _trianglesData;

	_verticesData = nullptr;
	_trianglesData = nullptr;
}

Cube::~Cube() {
}

MeshVertices Cube::createVertices()
{
	MeshVertices vertices;

	_verticesData = new float[6 * 8]{
		// Positions            //	Colors
		-0.5f,	 0.5f,	0.5f,	1.f,	1.f,	1.f,	// Front - Top - Left - Red
		-0.5f,	-0.5f,	0.5f,	1.f,	1.f,	1.f,	// Front - Bottom - Left - Green
		 0.5f,	-0.5f,	0.5f,	1.f,	1.f,	1.f,	// Front - Bottom - Right -	Blue
		 0.5f,	 0.5f,	0.5f,	1.f,	1.f,	1.f,	// Front - Top - Right - Yellow

		-0.5f,	 0.5f,	-0.5f,	1.f,	1.f,	1.f,	// Back - Top - Left - Mangeta
		-0.5f,	-0.5f,	-0.5f,	1.f,	1.f,	1.f,	// Back - Bottom - Left - Cyan
		 0.5f,	-0.5f,	-0.5f,	1.f,	1.f,	1.f,	// Back - Bottom - Right -	Red
		 0.5f,	 0.5f,	-0.5f,	1.f,	1.f,	1.f		// Back - Top - Right - Green
	};

	vertices._data = _verticesData;
	vertices._count = 6 * 8;

	// Define the vertex layout
	ge::VertexLayoutElement positions;
	positions.count = 3;
	positions.shaderAttrib = "position";

	ge::VertexLayoutElement color;
	color.count = 3;
	color.shaderAttrib = "color";

	vertices._layout.addElement(positions);
	vertices._layout.addElement(color);

	return vertices;
}

MeshTriangles Cube::createTriangles()
{
	MeshTriangles triangles;

	_trianglesData = new unsigned int[3 * 12]{
		// Front
		0, 1, 2,
		2, 3, 0,

		// Right
		3, 2, 6,
		6, 7, 3,

		// Back
		6, 5, 4,
		4, 7, 6,

		// Left
		5, 1, 0,
		0, 4, 5,

		// Top
		4, 0, 3,
		3, 7, 4,

		// Bottom
		2, 1, 5,
		5, 6, 2
	};

	triangles._data = _trianglesData;
	triangles._count = 3 * 12;

	return triangles;
}
