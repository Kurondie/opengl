#pragma once

class VertexBuffer {
public:
	VertexBuffer();
	~VertexBuffer();

	void bind() const;
	void setData(float* data, unsigned int count);
	void setData(float* data, unsigned int count, unsigned int offset);

	void allocate(unsigned int count);

private:
	unsigned int _vbo;
};

