#include "Light.h"

#include <string>

DirLight::DirLight() :
	_direction(0.f),
	_color(0.f)
{}

void DirLight::applyOnShader(const std::shared_ptr<ge::Shader>& shader, unsigned int lightNumber) {
	shader->setUniform("u_dirLigth.direction", _direction.x, _direction.y, _direction.y);
	shader->setUniform("u_dirLigth.color", _color.r, _color.g, _color.b);
}


PointLight::PointLight() : 
	_position(0.f),
	_color(0.f),
	_constant(0.f),
	_linear (0.f),
	_quadratic(0.f)
{}

void PointLight::applyOnShader(const std::shared_ptr<ge::Shader>& shader, unsigned int lightNumber) {
	shader->setUniform("u_lights[" + std::to_string(lightNumber) + "].position", _position.x, _position.y, _position.z);
	shader->setUniform("u_lights[" + std::to_string(lightNumber) + "].color", _color.r, _color.g, _color.b);
	shader->setUniform("u_lights[" + std::to_string(lightNumber) + "].constant", _constant);
	shader->setUniform("u_lights[" + std::to_string(lightNumber) + "].linear", _linear);
	shader->setUniform("u_lights[" + std::to_string(lightNumber) + "].quadratic", _quadratic);
}

