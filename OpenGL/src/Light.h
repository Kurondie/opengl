#pragma once

#include <glm/glm.hpp>

#include "shader.h"


class Light {
public:
	virtual void applyOnShader(const std::shared_ptr<ge::Shader>& shader, unsigned int lightNumber = 0) = 0;
};

class DirLight : Light {
public:
	DirLight();

	virtual void applyOnShader(const std::shared_ptr<ge::Shader>& shader, unsigned int lightNumber = 0) override;

public:
	glm::vec3 _direction;
	glm::vec3 _color;
};

class PointLight : public Light {
public:
	PointLight();

	virtual void applyOnShader(const std::shared_ptr<ge::Shader>& shader, unsigned int lightNumber = 0) override;

public:
	glm::vec3 _position;
	glm::vec3 _color;
	float _constant;
	float _linear;
	float _quadratic;
};