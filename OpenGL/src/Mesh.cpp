#include "Mesh.h"

#include <glm/gtc/matrix_transform.hpp>

Mesh::Mesh() : _transforms(1.f) {
}

Mesh::~Mesh() {
}

void Mesh::build() {
	// Create the buffers
	_vertexArray = std::make_shared<ge::VertexArray>();
	_vertexBuffer = std::make_shared<VertexBuffer>();
	_elementBuffer = std::make_shared<ge::ElementBuffer>();

	_vertexArray->bind();
	_vertexBuffer->bind();
	_elementBuffer->bind();

	// Fill the vertex buffer
	MeshVertices vertices = createVertices();

	_vertexBuffer->setData(vertices._data, vertices._count);
	_vertexArray->addVertexArray(_vertexBuffer, vertices._layout);

	// Fill the element buffer
	MeshTriangles triangles = createTriangles();

	_elementBuffer->setData(triangles._data, triangles._count);


	// Free memory
	/*delete[] vertices._data;
	delete[] triangles._data;

	vertices._data = nullptr;
	triangles._data = nullptr;*/
}

/*
 *	Transforms methods
 */

void Mesh::translate(const glm::vec3& position) {
	_transforms = glm::translate(_transforms, position);
}

void Mesh::rotate(const glm::vec3& axis, float angle) {
	_transforms = glm::rotate(_transforms, glm::radians(angle), axis);
}

void Mesh::scale(const glm::vec3& scale) {
	_transforms = glm::scale(_transforms, scale);
}

const glm::mat4& Mesh::getTransform() const {
	return _transforms;
}

void Mesh::draw() const {
	_vertexArray->bind();
	glDrawElements(GL_TRIANGLES, _elementBuffer->getCount(), GL_UNSIGNED_INT, 0);
}
