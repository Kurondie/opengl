#include "Terrain.h"

#include <glm/gtc/matrix_transform.hpp>

#include <random>

Terrain::Terrain(unsigned int n) :
	_width(std::pow(2.f, n) + 1),
	_height(std::pow(2.f, n) + 1)
{
	generateFlatTerain();
	initializeDiamondSquare();
	diamondSquareAlgorithm(0, 0, _width - 1, _height - 1);

	build();

	delete[] _vertices._data;
	delete[] _triangles._data;

	_vertices._data = nullptr;
	_triangles._data = nullptr;
}

Terrain::~Terrain() {}

void Terrain::generateFlatTerain() {
	_vertices._count = _width * _height * 6;
	_vertices._data = new float[_vertices._count];

	_triangles._count = ((int)_width - 1) * ((int)_height - 1) * 2 * 3;
	_triangles._data = new unsigned int[_triangles._count];

	/*
		Create a flat terrain with the given vertex number
			i : vertex number
			x : X position
			z : Z position
	*/
	unsigned int elementIndex = 0;

	for (unsigned int i = 0; i < _width * _height; i++) {
		unsigned int x = i % _width;
		unsigned int z = i / _width;
		
		// Create the vertices
		_vertices._data[i * 6] = (float)x;
		_vertices._data[i * 6 + 1] = 0.f;
		_vertices._data[i * 6 + 2] = (float)z;

		_vertices._data[i * 6 + 3] = 0.20f;
		_vertices._data[i * 6 + 4] = 0.43f;
		_vertices._data[i * 6 + 5] = 0.08f;

		// Create the triangles
		if (z >= 1) {
			if (x == 0) {
				_triangles._data[elementIndex] = i;
				_triangles._data[elementIndex + 2] = i - _width;
				_triangles._data[elementIndex + 1] = i - _width + 1;

				elementIndex += 3;
			}
			else if (x == _width - 1) {
				_triangles._data[elementIndex] = i;
				_triangles._data[elementIndex + 2] = i - 1;
				_triangles._data[elementIndex + 1] = i - _width;

				elementIndex += 3;
			}
			else {
				_triangles._data[elementIndex] = i;
				_triangles._data[elementIndex + 2] = i - 1;
				_triangles._data[elementIndex + 1] = i - _width;

				_triangles._data[elementIndex + 3] = i;
				_triangles._data[elementIndex + 5] = i - _width;
				_triangles._data[elementIndex + 4] = i - _width + 1;

				elementIndex += 6;
			}
		}
	}
}

void Terrain::initializeDiamondSquare() {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<float> random(0.f, 10.f);

	unsigned int x, z;

	// Top left
	x = 0;
	z = 0;

	_vertices._data[(z * _width + x) * 6 + 1] = random(gen);
	
	// Top right
	x = _width - 1;
	z = 0;
	_vertices._data[(z * _width + x) * 6 + 1] = random(gen);

	// Bottom left
	x = 0;
	z = _height - 1;
	_vertices._data[(z * _width + x) * 6 + 1] = random(gen);

	// Bottom right
	x = _width - 1;
	z = _height - 1;
	_vertices._data[(z * _width + x) * 6 + 1] = random(gen);
}

void Terrain::diamondSquareAlgorithm(unsigned int x1, unsigned int z1, unsigned int x2, unsigned int z2) {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<float> random(0.f, 0.1f);

	if (x1 + 1 == x2)
		return;

	// The 4 corner values
	float y1 = _vertices._data[(z1 * _width + x1) * 6 + 1];
	float y2 = _vertices._data[(z2 * _width + x2) * 6 + 1];
	float y3 = _vertices._data[(z2 * _width + x1) * 6 + 1];
	float y4 = _vertices._data[(z1 * _width + x2) * 6 + 1];


	// Square 
	float avg = (y1 + y2 + y3 + y4) / 4;

	unsigned int x3 = x1 + (x2 - x1) / 2;
	unsigned int z3 = z1 + (z2 - z1) / 2;

	_vertices._data[(z3 * _width + x3) * 6 + 1] = avg + random(gen);

	// Diamond
	y1 = _vertices._data[(z1 * _width + x1) * 6 + 1];
	y2 = _vertices._data[(z2 * _width + x1) * 6 + 1];
	y3 = _vertices._data[(z3 * _width + x3) * 6 + 1];
	
	avg = (y1 + y2 + y3) / 3;
	_vertices._data[(z3 * _width + x1) * 6 + 1] = avg + random(gen);


	y1 = _vertices._data[(z1 * _width + x1) * 6 + 1];
	y2 = _vertices._data[(z1 * _width + x2) * 6 + 1];
	y3 = _vertices._data[(z3 * _width + x3) * 6 + 1];

	avg = (y1 + y2 + y3) / 3;
	_vertices._data[(z1 * _width + x3) * 6 + 1] = avg + random(gen);


	y1 = _vertices._data[(z2 * _width + x1) * 6 + 1];
	y2 = _vertices._data[(z2 * _width + x2) * 6 + 1];
	y3 = _vertices._data[(z3 * _width + x3) * 6 + 1];

	avg = (y1 + y2 + y3) / 3;
	_vertices._data[(z2 * _width + x3) * 6 + 1] = avg + random(gen);


	y1 = _vertices._data[(z1 * _width + x2) * 6 + 1];
	y2 = _vertices._data[(z2 * _width + x2) * 6 + 1];
	y3 = _vertices._data[(z3 * _width + x3) * 6 + 1];

	avg = (y1 + y2 + y3) / 3;
	_vertices._data[(z3 * _width + x2) * 6 + 1] = avg + random(gen);

	diamondSquareAlgorithm(x1, z1, x3,z3);
	diamondSquareAlgorithm(x3, z1, x2, z3);
	diamondSquareAlgorithm(x1, z3, x3, z2);
	diamondSquareAlgorithm(x3, z3, x2, z2);
}

MeshVertices Terrain::createVertices()
{
	ge::VertexLayoutElement position;
	position.count = 3;
	position.shaderAttrib = "position";

	ge::VertexLayoutElement color;
	color.count = 3;
	color.shaderAttrib = "color";

	_vertices._layout.addElement(position);
	_vertices._layout.addElement(color);

	return _vertices;
}

MeshTriangles Terrain::createTriangles()
{
	return _triangles;
}