#include "utils.h"

#include <fstream>
#include <string>

std::string getFileContent(const std::string& fileName) {
    std::ifstream file(fileName.c_str());
    std::string ret = "";

    if (file.is_open()) {
        std::string line;
        while (getline(file, line)) {
            ret.append(line);
            ret.append("\n");
        }

        file.close();
    }
    else {
        std::cerr << "Error reading file " << fileName << std::endl;
    }

    return ret;
}
