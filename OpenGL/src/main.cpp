#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>

#include "Window.h"
#include "OrthographicCamera.h"

#include "Renderer.h"
#include "Shape.h"

int main(void) {
	// Initialize GLFW
	if (!glfwInit())
		std::cerr << "Enable to initialize GLFW." << std::endl;

	Window window(800, 450, "OpenGL");

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);

	// Create a camera
	OrthographicCamera camera(0.f, 16.f, 0.f, 9.f, -1.f, 1.f);
	ge::Renderer renderer(camera);

	// Creates shapes to render
	ge::Shape square1(2.f, 2.f, 1.f, 1.f);
	ge::Shape square2(4.f, 4.f, 2.f, 1.f);
	ge::Shape square3(10.f, 1.f, 2.f, 3.f);

	// Clear screen in black
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

	float before = 0.f;
	float now = 0.f;
	float delta = 0.f;

	// Game loop
	while (window.isOpen()) {
		// Compute delta time
		before = now;
		now = (float) glfwGetTime();
		delta = now - before;

		// Draw
		window.clear();

		renderer.draw(square1);
		renderer.draw(square2);
		renderer.draw(square3);

		renderer.flush();

		window.update();
	}

	glfwTerminate();

	return 0;
}