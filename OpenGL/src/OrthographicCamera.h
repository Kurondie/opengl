#pragma once

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

class OrthographicCamera {
public:
	OrthographicCamera(float left, float right, float top, float bottom, float near, float far);
	~OrthographicCamera();

	void setPosition(const glm::vec3& position);
	const glm::vec3& getPosition() const;

	void setRotation(const glm::vec3& axis, float angle);
	const glm::quat& getRotation() const;

	void translate(const glm::vec3& move);
	void rotate(const glm::vec3& axis, float angle);

	const glm::mat4& getProjectionView() const;


private:
	void update();

private:
	float _left, _right, _top, _bottom, _near, _far;

	glm::mat4 _projection;
	glm::mat4 _view;
	glm::mat4 _projectionView;

	glm::vec3 _position;
	glm::quat _rotations;
};

