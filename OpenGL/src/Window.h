#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>
#include <unordered_map>

class Window {
public:
	Window(unsigned int width, unsigned int height, const std::string& title);
	~Window();

	bool isOpen() const;
	void clear() const;
	void update() const;

	bool isKeyPressed(int key) const;

private:
	friend static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

	GLFWwindow* _window;

	unsigned int _width;
	unsigned int _height;

	std::string _title;

	std::unordered_map<int, bool> _keyPressed;
};

