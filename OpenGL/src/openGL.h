#pragma once

#include <GL/glew.h>
#include <iostream>

void clearErrors() {
	while (glGetError() != GL_NO_ERROR);
}

void checkError() {
	while (GLenum error = glGetError() != GL_NO_ERROR) {
		std::cerr << "[Open GL error] : " << error << std::endl;
	}
}
