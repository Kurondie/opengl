#pragma once

#include "shader.h"

#include "VertexBuffer.h"
#include "ElementBuffer.h"
#include "VertexArray.h"

#include "OrthographicCamera.h"

#include "Shape.h"

constexpr auto MAX_VERTEX = 1000;
constexpr auto MAX_INDICE = 1000;

namespace ge {
	
enum class RenderMode {
	Triangles,
	Lines
};

class Renderer {
public:

	Renderer(const OrthographicCamera& camera);
	~Renderer();

	void draw(const Shape& shape);
	void draw(float* data, unsigned int count);

	void flush();

private:
	unsigned int _shapeStartingVertex;
	unsigned int _currentVertex;
	unsigned int _currentIndice;

	std::shared_ptr<Shader> _shader;

	VertexLayout _layout;
	std::shared_ptr<VertexArray> _vertexArray;
	std::shared_ptr<VertexBuffer> _vertexBuffer;
	std::shared_ptr<ElementBuffer> _elementBuffer;

	const OrthographicCamera _camera;
};

} //ge