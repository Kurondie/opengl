#pragma once

#include <memory>

#include "VertexArray.h"
#include "VertexBuffer.h"
#include "ElementBuffer.h"

struct MeshVertices {
	float* _data;
	unsigned int _count;
	ge::VertexLayout _layout;
};

struct MeshTriangles {
	unsigned int* _data;
	unsigned int _count;
};

class Mesh {
public:
	Mesh();
	virtual ~Mesh();

	void build();

	void translate(const glm::vec3& position);
	void rotate(const glm::vec3& axis, float angle);
	void scale(const glm::vec3& scale);

	const glm::mat4& getTransform() const;

	void draw() const;

private:
	virtual MeshVertices createVertices() = 0;
	virtual MeshTriangles createTriangles() = 0;

private:
	std::shared_ptr<ge::VertexArray> _vertexArray;
	std::shared_ptr<VertexBuffer> _vertexBuffer;
	std::shared_ptr<ge::ElementBuffer> _elementBuffer;

	glm::mat4 _transforms;
};