#include "Renderer.h"

#include <vector>

namespace ge {

Renderer::Renderer(const OrthographicCamera& camera) :
	_currentVertex(0),
	_currentIndice(0),
	_shapeStartingVertex(0),
	_camera(camera)
{
	// Creates the shader program
	_shader = std::make_shared<ge::Shader>("res/shader/basic.vert", "res/shader/basic.frag");

	// Creates the buffers
	_vertexArray = std::make_shared<ge::VertexArray>();
	_vertexBuffer = std::make_shared<VertexBuffer>();
	_elementBuffer = std::make_shared<ge::ElementBuffer>();

	// Defines the layout
	VertexLayoutElement positions;
	positions.count = 2;
	positions.shaderAttrib = "position";

	VertexLayoutElement color;
	color.count = 4;
	color.shaderAttrib = "color";

	_layout.addElement(positions);
	_layout.addElement(color);

	// Binds the buffers
	_vertexArray->bind();
	_vertexBuffer->bind();
	_elementBuffer->bind();

	// Allocates 1000 vertices
	_vertexBuffer->allocate(MAX_VERTEX * _layout.getCount());
	_vertexArray->addVertexArray(_vertexBuffer, _layout);
	_elementBuffer->allocate(MAX_INDICE);
}

Renderer::~Renderer() {}

void Renderer::draw(const Shape& shape) {
	shape.draw(*this);
}

void Renderer::draw(float* data, unsigned int count) {
	unsigned int max = (count % 2) == 0 ? (count / 2) + 1 : count / 2;

	std::vector<unsigned int> indicesOrder;
	std::vector<unsigned int> indices;

	// Orders the indices
	indicesOrder.push_back(0);
	indicesOrder.push_back(1);
	for (unsigned int i = 0; i <= max; i++) {
		if (i % 2 == 0) {
			indicesOrder.push_back(count - 1 - (i / 2));
		}
		else {
			indicesOrder.push_back((i / 2) + 2);
		}
	}

	// Fills the indices array to form a triangle strip of the shape
	for (unsigned int i = 0; i < count - 2; i++) {
		if (i % 2 == 0) {
			indices.push_back(indicesOrder[i]);
			indices.push_back(indicesOrder[i + 1]);
			indices.push_back(indicesOrder[i + 2]);
		}
		else {
			indices.push_back(indicesOrder[i + 2]);
			indices.push_back(indicesOrder[i + 1]);
			indices.push_back(indicesOrder[i]);
		}
	}

	// Check if the batch need to be flush
	if (_currentVertex + count * _layout.getCount() > MAX_VERTEX) {
		flush();
	}

	if (_currentIndice + count > MAX_INDICE) {
		flush();
	}

	// Make the indices match the added vertex
	for (unsigned int i = 0; i < (count - 2) * 3; i++) {
		indices[i] += _currentVertex;
	}

	_vertexBuffer->setData(data, count * _layout.getCount(), _currentVertex * _layout.getCount());
	_currentVertex += count;

	_elementBuffer->setData(&indices[0], (count - 2) * 3, _currentIndice);
	_currentIndice += (count - 2) * 3;
}

void Renderer::flush() {
	// Sends shader uniforms
	_shader->setUniformMat4("u_vp", _camera.getProjectionView());

	// Binds the vertex array
	_vertexArray->bind();

	// Draw
	glDrawElements(GL_TRIANGLES, _currentIndice, GL_UNSIGNED_INT, 0);
	
	_currentVertex = 0;
	_currentIndice = 0;
}

} // ge
