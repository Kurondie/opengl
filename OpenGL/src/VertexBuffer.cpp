#include "VertexBuffer.h"

#include <GL/glew.h>

#include <iostream>

VertexBuffer::VertexBuffer() {
	glGenBuffers(1, &_vbo);
}

VertexBuffer::~VertexBuffer() {
	glDeleteBuffers(1, &_vbo);
}

void VertexBuffer::bind() const {
	glBindBuffer(GL_ARRAY_BUFFER, _vbo);
}

void VertexBuffer::setData(float* data, unsigned int count) {
	glBufferData(GL_ARRAY_BUFFER, count * sizeof(float), data, GL_STATIC_DRAW);
}

void VertexBuffer::setData(float* data, unsigned int count, unsigned int offset) {
	bind();
	glBufferSubData(GL_ARRAY_BUFFER, offset * sizeof(float), count * sizeof(float), data);
}

void VertexBuffer::allocate(unsigned int count) {
	glBufferData(GL_ARRAY_BUFFER, count * sizeof(float), nullptr, GL_DYNAMIC_DRAW);

}
