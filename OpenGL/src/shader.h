#pragma once
#include <GL/glew.h>
#include <iostream>

#include <glm/glm.hpp>

namespace ge {

class Shader
{
public:
    Shader(const std::string& vertexShader, const std::string& fragmentShader, const std::string& geometryShader = "");
    ~Shader();

    void setUniform(const std::string& name, float f1, float f2, float f3);
	void setUniform(const std::string& name, float f);
	void setUniformMat4(const std::string& name, const glm::mat4& mat);

    unsigned int getProgramID() const;

private:
    void addShader(GLuint shader, const std::string& shaderContent);

    std::string _content;

    unsigned int _shaderProgram;

    unsigned int _vertexShader;
    unsigned int _fragmentShader;
	unsigned int _geometryShader;
};

} // ge
