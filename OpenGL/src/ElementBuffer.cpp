#include "ElementBuffer.h"

#include <GL/glew.h>

namespace ge {

ElementBuffer::ElementBuffer() : _count(0) {
	glGenBuffers(1, &_ebo);
}

ElementBuffer::~ElementBuffer() {
	glDeleteBuffers(1, &_ebo);
}

void ElementBuffer::bind() {
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
}

void ElementBuffer::setData(unsigned int* data, unsigned int count) {
	_count = count;
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned int), data, GL_STATIC_DRAW);
}

void ElementBuffer::setData(unsigned int* data, unsigned int count, unsigned int offset) {
	bind();
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset * sizeof(unsigned int), count * sizeof(unsigned int), data);
}

void ElementBuffer::allocate(unsigned int count) {
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned int), nullptr, GL_DYNAMIC_DRAW);
}

unsigned int ElementBuffer::getCount() const
{
	return _count;
}

} // ge
