#pragma once

#include "Mesh.h"

#include <memory>

#include "VertexArray.h"
#include "VertexBuffer.h"
#include "ElementBuffer.h"


class Cube : public Mesh {
public:
	Cube();
	~Cube();

private:
	virtual MeshVertices createVertices() override;
	virtual MeshTriangles createTriangles() override;

private:
	float* _verticesData;
	unsigned int* _trianglesData;
};

