#pragma once

#include <unordered_map>
#include "Shader.h"


class Material {

private:
	ge::Shader _shader;
	std::unordered_map<std::string, float> uniforms;
};

