#include "Shape.h"

#include "Renderer.h"

namespace ge {

Shape::Shape(float x, float y, float width, float height) {
	// Creates the vertices
	_vertices = new float[4 * 6] {
		x,			y,			1.f,	1.f,	1.f,	1.f,
		x,			y + height,	1.f,	1.f,	1.f,	1.f,
		x + width,	y + height,	1.f,	1.f,	1.f,	1.f,
		x + width,	y,			1.f,	1.f,	1.f,	1.f
	};
}

Shape::~Shape() {
	delete[] _vertices;
}
/*Shape::Shape() {
	_vertices = new float[1];
}

void Shape::update() {
	delete[] _vertices;
	unsigned int vertexCount = getVertexCount();
	unsigned int stride = 6; // TODO : use vertex layout

	_vertices = new float[vertexCount];

	for (unsigned int i = 0; i < vertexCount; i++) {
		float* vertexData = getVertex(i);

		for (unsigned int j = 0; j < stride; j++) {
			_vertices[i * stride + j] = vertexData[j];
		}
	}
}*/

void Shape::draw(Renderer& renderer) const {
	renderer.draw(_vertices, 4);
}



/*Rectangle::Rectangle(float x, float y, float width, float height) {
	_vertices2 = new float[4 * 6] {
		x, y, 1.f, 1.f, 1.f, 1.f,
			x, y + height, 1.f, 1.f, 1.f, 1.f,
			x + width, y + height, 1.f, 1.f, 1.f, 1.f,
			x + width, y, 1.f, 1.f, 1.f, 1.f
	};

	update();
}

Rectangle::~Rectangle() {
	delete[] _vertices2;
}

unsigned int Rectangle::getVertexCount() const
{
	return 4;
}

float* Rectangle::getVertex(unsigned int index) const
{
	return _vertices2 + 6 * index;
}*/

} // ge