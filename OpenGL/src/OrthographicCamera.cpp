#include "OrthographicCamera.h"

OrthographicCamera::OrthographicCamera(float left, float right, float top, float bottom, float near, float far) :
	_left(left),
	_right(right),
	_top(top),
	_bottom(bottom),
	_near(near),
	_far(far),

	_projection(glm::ortho(_left, _right, _bottom, _top, _near, _far)),
	_view(1.f),
	_projectionView(_projection),

	_position(0.f),
	_rotations(0.f, 0.f, 0.f, 0.f)
{
	update();
}

OrthographicCamera::~OrthographicCamera() {}


void OrthographicCamera::setPosition(const glm::vec3& position) {
	_position = position;
	update();
}

const glm::vec3& OrthographicCamera::getPosition() const {
	return _position;
}

void OrthographicCamera::translate(const glm::vec3& move) {
	_position += move;
	update();
}


void OrthographicCamera::setRotation(const glm::vec3& axis, float angle) {
	_rotations = glm::angleAxis(glm::radians(angle), axis);
	update();
}

const glm::quat& OrthographicCamera::getRotation() const {
	return _rotations;
}

void OrthographicCamera::rotate(const glm::vec3& axis, float angle) {
	_rotations = glm::rotate(_rotations, glm::radians(angle), axis);
	update();
}


const glm::mat4& OrthographicCamera::getProjectionView() const {
	return _projectionView;
}


void OrthographicCamera::update() {
	glm::mat4 transform = glm::translate(glm::mat4(1.f), _position);
	transform = transform * glm::toMat4(_rotations);
	_view = glm::inverse(transform);


	_projectionView = _projection * _view;
}
