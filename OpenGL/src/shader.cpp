#include "shader.h"

#include "utils.h"

#include <iostream>

namespace ge {

Shader::Shader(const std::string& vertexShader, const std::string& fragmentShader, const std::string& geometryShader) {
    _shaderProgram = glCreateProgram();

    // Check if the shader program have been created properly
    if (!_shaderProgram) {
        std::cerr << "Error creating the shader program" << std::endl;
        exit(1);
    }

    std::string vertexShaderContent, fragmentShaderContent, geometryShaderContent;

    // Load vertex shader content
    vertexShaderContent = getFileContent(vertexShader);
    if (vertexShaderContent == "") {
        exit(1);
    }

    // Load fragment shader content
    fragmentShaderContent = getFileContent(fragmentShader);
    if (fragmentShaderContent == "") {
        exit(1);
    }

    // Add the vertex shader
    _vertexShader = glCreateShader(GL_VERTEX_SHADER);
    if (_vertexShader == 0) {
        std::cerr << "Error creating the vertex shader" << std::endl;
        exit(1);
    }
    addShader(_vertexShader, vertexShaderContent);

	// Check if the geometry shader is specified
	if (geometryShader != "") {
		// Load geometry shader
		geometryShaderContent = getFileContent(geometryShader);
		if (geometryShaderContent == "") {
			exit(1);
		}

		_geometryShader = glCreateShader(GL_GEOMETRY_SHADER);
		if (_geometryShader == 0) {
			std::cerr << "Error creating the geometry shader" << std::endl;
		}

		addShader(_geometryShader, geometryShaderContent);
	}

    // Add the fragment shader
    _fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    if (_fragmentShader == 0) {
        std::cerr << "Error creating the vertex shader" << std::endl;
        exit(1);
    }
    addShader(_fragmentShader, fragmentShaderContent);



    GLint success = 0;
    GLchar errorLog[1024] = { 0 };

    // Link the program
    glLinkProgram(_shaderProgram);
    glGetProgramiv(_shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(_shaderProgram, sizeof(errorLog), NULL, errorLog);
        std::cerr << "Error linking the shader program : " << errorLog << std::endl;
        exit(1);
    }

    // Validate the program
    glValidateProgram(_shaderProgram);
    glGetProgramiv(_shaderProgram, GL_VALIDATE_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(_shaderProgram, sizeof(errorLog), NULL, errorLog);
        std::cerr << "Invalid shader program : " << errorLog << std::endl;
        exit(1);
    }

    glUseProgram(_shaderProgram);
}

Shader::~Shader()
{
    glDeleteProgram(_shaderProgram);
    glDeleteShader(_fragmentShader);
    glDeleteShader(_vertexShader);
}

void Shader::setUniform(const std::string& name, float f1, float f2, float f3) {
    GLint uniLocation = glGetUniformLocation(_shaderProgram, name.c_str());

    glUniform3f(uniLocation, f1, f2, f3);
}

void Shader::setUniform(const std::string& name, float f) {
	GLint uniLocation = glGetUniformLocation(_shaderProgram, name.c_str());

	glUniform1f(uniLocation, f);
}

void Shader::setUniformMat4(const std::string& name, const glm::mat4& mat) {
	GLint uniLocation = glGetUniformLocation(_shaderProgram, name.c_str());

	glUniformMatrix4fv(uniLocation, 1, GL_FALSE, &mat[0][0]);
}

unsigned int Shader::getProgramID() const {
    return _shaderProgram;
}


void Shader::addShader(GLuint shader, const std::string& shaderContent) {
    // Add the source to the shader
    const GLchar* p[1];
    GLint length[1];
    p[0] = shaderContent.c_str();
    length[0] = shaderContent.length();
    glShaderSource(shader, 1, p, length);

    // Compile the shader
    GLint success;
    glCompileShader(shader);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        GLchar infoLog[1024];
        glGetShaderInfoLog(shader, 1024, NULL, infoLog);
        std::cerr << "Error compiling shader : " << infoLog << std::endl;
        exit(1);
    }

    // attach the shader to the program
    glAttachShader(_shaderProgram, shader);
}

} // ge
