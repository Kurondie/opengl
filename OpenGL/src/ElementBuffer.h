#pragma once

namespace ge {

	class ElementBuffer {
	public:
		ElementBuffer();
		~ElementBuffer();

		void bind();
		void setData(unsigned int* data, unsigned int count);
		void setData(unsigned int* data, unsigned int count, unsigned int offset);

		void allocate(unsigned int count);

		unsigned int getCount() const;

	private:
		unsigned int _ebo;
		unsigned int _count;
	};

} // ge