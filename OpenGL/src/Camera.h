#pragma once

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

class Camera {
public:
	Camera(float fov, float aspect, float near, float far);
	~Camera();

	void setPosition(const glm::vec3& position);
	const glm::vec3& getPosition() const;

	void setRotation(const glm::vec3& axis, float angle);

	void translate(const glm::vec3& move);
	void rotate(const glm::vec3& axis, float angle);

	const glm::mat4& getProjectionView() const;

	const glm::quat& getQuat() const;

private:
	void update();

private:
	float _fov;
	float _aspect;
	float _near;
	float _far;

	glm::mat4 _projection;
	glm::mat4 _view;
	glm::mat4 _projectionView;

	glm::vec3 _position;
	glm::quat _rotations;


	//glm::vec3 _rotationAxis;
	//float _angle;
};

