#include "Window.h"

#include <iostream>

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

Window::Window(unsigned int width, unsigned int height, const std::string& title) {
	_width = width;
	_height = height;
	_title = title;
	
	// Create a GLFW window
	_window = glfwCreateWindow(_width, _height, _title.c_str(), NULL, NULL);
	if (!_window) {
		std::cerr << "Enable to creates a window" << std::endl;
		glfwTerminate();
	}

	// Make the window's context current
	glfwMakeContextCurrent(_window);

	// Register our class in
	glfwSetWindowUserPointer(_window, this);

	// Set callbacks
	glfwSetKeyCallback(_window, key_callback);

	// Initialize OpenGL's states with GLEW
	GLenum res = glewInit();
	if (res != GLEW_OK) {
		std::cerr << "Error: " << glewGetErrorString(res) << std::endl;
	}
}

Window::~Window() {
	glfwDestroyWindow(_window);
}

bool Window::isOpen() const {
	return !glfwWindowShouldClose(_window);
}

void Window::clear() const {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Window::update() const {
	// Swap buffers
	glfwSwapBuffers(_window);
	glfwPollEvents();
}

bool Window::isKeyPressed(int key) const {
	if (_keyPressed.find(key) == _keyPressed.end())
		return false;

	return _keyPressed.at(key);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	Window* win = (Window*) glfwGetWindowUserPointer(window);

	win->_keyPressed[key] = action == GLFW_PRESS || action == GLFW_REPEAT;
}
