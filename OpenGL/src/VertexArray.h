#pragma once

#include "shader.h"
#include "VertexBuffer.h"

#include <vector>

namespace ge {

class VertexLayoutElement {
public:
	std::string shaderAttrib;
	unsigned int count;
};

class VertexLayout {
public:
	VertexLayout();

	void addElement(const VertexLayoutElement& element);

	const std::vector<VertexLayoutElement>& getElements() const;
	unsigned int getStride() const;
	unsigned int getCount() const;

private:
	std::vector<VertexLayoutElement> _elements;
	unsigned int _stride;
};

class VertexArray {
public:
	VertexArray();
	~VertexArray();

	void bind() const;
	void unbind() const;
	void addVertexArray(const std::shared_ptr<VertexBuffer>& vb, const VertexLayout& layout);
	unsigned int getID() const;

private:
	unsigned int _vao;
};

} // ge