#pragma once

//#include "Renderer.h"

namespace ge {

class Renderer;

class Shape {
public:
	Shape(float x, float y, float width, float height);
	virtual ~Shape();

	//virtual unsigned int getVertexCount() const = 0;
	//virtual float* getVertex(unsigned int index) const = 0;

/*protected:
	Shape();
	void update();*/

private:
	friend class Renderer;
	void draw(Renderer& renderer) const;

private:

	float* _vertices;
	//unsigned int _vertexCount;
	//unsigned int* _indices;
	//unsigned int _indiceCount;
};


/*class Rectangle : public Shape {
public:
	Rectangle(float x, float y, float width, float height);
	~Rectangle();

	virtual unsigned int getVertexCount() const;
	virtual float* getVertex(unsigned int index) const;

private:
	float* _vertices2;
};*/

} // ge