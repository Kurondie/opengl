#version 330

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;

uniform mat4 u_vp;

out vec3 fs_color;

void main()
{
	mat4 mvp = u_vp;

	fs_color = color;

    gl_Position = mvp * vec4(position, 1.0);
}