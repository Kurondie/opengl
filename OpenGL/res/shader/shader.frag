#version 330

#define MAX_LIGHT_NB 100

struct PointLight {
	vec3 position;
	vec3 color;
	float constant;
	float linear;
	float quadratic;
};

struct DirLight {
	vec3 direction;
	vec3 color;
};

in vec3 fs_color;
in vec3 fs_fragPos;
in vec3 fs_normal;

uniform DirLight u_dirLigth;
uniform PointLight u_lights[MAX_LIGHT_NB];
uniform float u_ligthCount;

uniform vec3 u_viewPosition;

out vec4 FragColor;

// Calculates the color when using a point light.
vec3 pointLightCalculation(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir) {
	float ambientStrength = 0.1;
	float specularStrength = 0.5;

	vec3 diffuse = vec3(0.0);
	vec3 ambient = vec3(0.0);
	vec3 specular = vec3(0.0);

	// Apply the ambient color for the current ligth
	ambient = ambientStrength * light.color;
		

	// Define the diffuse color
	vec3 lightDir = normalize(light.position - fs_fragPos);  
	float diffuseStrengh = max(dot(normal, lightDir), 0.0);
	diffuse = diffuseStrengh * light.color;

	// Define the attenuation
	float distance = length(light.position - fs_fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * distance * distance);

	diffuse *= attenuation;


	// Define specular color
	vec3 reflectDir = reflect(-lightDir, normal);  

	// 32 : shininess
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
	specular = specularStrength * spec * light.color;  

	return (ambient + diffuse + specular) * fs_color;
}

// calculates the color when using a directional light.
vec3 dirLightCalculation(DirLight light, vec3 normal, vec3 viewDir) {
    vec3 lightDir = normalize(-light.direction);

    // Define the diffuse color
    float diff = max(dot(normal, lightDir), 0.0);

    // Define specular color
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);

    // combine results
    vec3 ambient = light.color * 0.5;
    vec3 diffuse = light.color * diff;
    vec3 specular = light.color * spec;

   return (ambient + diffuse + specular) * fs_color;
   //return -light.direction;
}

void main()
{
	vec3 result = vec3(0.0);
	vec3 viewDir = normalize(u_viewPosition - fs_fragPos);

	// Compute the directional ligth
	result += dirLightCalculation(u_dirLigth, fs_normal, viewDir);

	// Compute all point ligths
	int i;
	for (i = 0; i < u_ligthCount; i++) {
		result += pointLightCalculation(u_lights[i], fs_normal, fs_fragPos, viewDir);
	}

	FragColor = vec4(result, 1.0);
}
