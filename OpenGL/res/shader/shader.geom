#version 430 core

#define MAX_LIGHT_NB 100

struct PointLigth {
	vec3 position;
	vec4 color;
	float constant;
	float linear;
	float quadratic;
};

layout (triangles) in;
layout (triangle_strip) out;
layout (max_vertices = 3) out;

in vec3 gs_color[];
in vec3 gs_fragPos[];

out vec3 fs_color;
out vec3 fs_fragPos;
out vec3 fs_normal;

void main(void)
{
    int i;

	// Compute normal of the face
	vec3 a = vec3(gs_fragPos[0]) - vec3(gs_fragPos[1]); 
	vec3 b = vec3(gs_fragPos[0]) - vec3(gs_fragPos[2]); 
	fs_normal = normalize(cross(a, b));


    for (i = 0; i < gl_in.length(); i++)
    {
		fs_color = gs_color[i];
		fs_fragPos = gs_fragPos[i];
		gl_Position = gl_in[i].gl_Position;

		EmitVertex();
   }

   EndPrimitive();
	
}