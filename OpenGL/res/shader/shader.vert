#version 330

#define MAX_LIGHT_NB 100

struct PointLigth {
	vec3 position;
	vec4 color;
	float constant;
	float linear;
	float quadratic;
};

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;

uniform mat4 u_vp;
uniform mat4 u_model;

uniform PointLigth u_lights[MAX_LIGHT_NB];
uniform float u_ligthCount;


out vec3 gs_color;
out vec3 gs_fragPos;

void main()
{
	mat4 mvp = u_vp * u_model;

	gs_color = color;
	gs_fragPos = vec3((u_model * vec4(position, 1.0)));

    gl_Position = mvp * vec4(position, 1.0);
}
